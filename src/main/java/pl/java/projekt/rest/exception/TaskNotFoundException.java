package pl.java.projekt.rest.exception;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public class TaskNotFoundException extends Exception {

    public TaskNotFoundException(String reason) {
        super(reason);
    }

    public TaskNotFoundException(Long id) {
        super("Task with id=" + id + " doesn't exist.");
    }

}
