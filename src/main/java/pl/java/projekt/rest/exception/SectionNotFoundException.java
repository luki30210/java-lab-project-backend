package pl.java.projekt.rest.exception;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public class SectionNotFoundException extends Exception {

    public SectionNotFoundException(String reason) {
        super(reason);
    }

    public SectionNotFoundException(Long id) {
        super("Section with id=" + id + " doesn't exist.");
    }

}
