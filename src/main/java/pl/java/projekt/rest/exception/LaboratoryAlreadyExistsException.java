package pl.java.projekt.rest.exception;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public class LaboratoryAlreadyExistsException extends Exception {

    public LaboratoryAlreadyExistsException(String reason) {
        super(reason);
    }

    public LaboratoryAlreadyExistsException(Long number) {
        super("Laboratory " + number + " already exists.");
    }

}
