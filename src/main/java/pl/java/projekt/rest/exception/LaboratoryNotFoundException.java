package pl.java.projekt.rest.exception;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public class LaboratoryNotFoundException extends Exception {

    public LaboratoryNotFoundException(String reason) {
        super(reason);
    }

    public LaboratoryNotFoundException(Long id) {
        super("Laboratory with id=" + id + " doesn't exist.");
    }

}
