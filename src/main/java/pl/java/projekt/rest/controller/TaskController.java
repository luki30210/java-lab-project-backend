package pl.java.projekt.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.exception.TaskNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.Task;
import pl.java.projekt.rest.model.service.LaboratoryService;
import pl.java.projekt.rest.model.service.TaskService;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
@RestController
@CrossOrigin
@RequestMapping("/laboratory/{laboratoryNumber}/task")
public class TaskController {

    @Autowired
    LaboratoryService laboratoryService;

    @Autowired
    TaskService taskService;

    @PostMapping
    public ResponseEntity<Task> addTask(@RequestBody Task task, @PathVariable Long laboratoryNumber) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            task.setLaboratory(laboratory);
            taskService.addNewTask(task);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping
    public ResponseEntity<List<Task>> getAllTasks(@PathVariable Long laboratoryNumber) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            List<Task> tasks = taskService.getAllTasksForLaboratory(laboratory);
            return new ResponseEntity<>(tasks, HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (TaskNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/{taskId}")
    public ResponseEntity<Task> getTask(@PathVariable Long taskId, @PathVariable Long laboratoryNumber) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            return new ResponseEntity<>(task, HttpStatus.OK);
        } catch (TaskNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/{taskId}")
    public ResponseEntity<Task> updateTask(@RequestBody Task task, @PathVariable Long laboratoryNumber, @PathVariable Long taskId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            task.setLaboratory(laboratory);
            task.setId(taskId);
            taskService.updateTask(task);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TaskNotFoundException | LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/{taskId}")
    public ResponseEntity<Task> deleteTask(@PathVariable Long taskId, @PathVariable Long laboratoryNumber) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            taskService.deleteTask(task.getId());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TaskNotFoundException | LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
