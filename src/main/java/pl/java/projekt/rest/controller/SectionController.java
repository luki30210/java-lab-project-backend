package pl.java.projekt.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.exception.SectionNotFoundException;
import pl.java.projekt.rest.exception.TaskNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.Section;
import pl.java.projekt.rest.model.Task;
import pl.java.projekt.rest.model.service.LaboratoryService;
import pl.java.projekt.rest.model.service.SectionService;
import pl.java.projekt.rest.model.service.TaskService;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
@RestController
@CrossOrigin
@RequestMapping("/laboratory/{laboratoryNumber}/task/{taskId}/section")
public class SectionController {

    @Autowired
    LaboratoryService laboratoryService;

    @Autowired
    TaskService taskService;

    @Autowired
    SectionService sectionService;

    @PostMapping
    public ResponseEntity<Section> addSection(@RequestBody Section section, @PathVariable Long laboratoryNumber, @PathVariable Long taskId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            section.setTask(task);
            sectionService.addNewSection(section);
            return new ResponseEntity<>(section, HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (TaskNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping
    public ResponseEntity<List<Section>> getAllSections(@PathVariable Long laboratoryNumber, @PathVariable Long taskId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            List<Section> sections = sectionService.getAllSectionsFromTask(task);
            return new ResponseEntity<>(sections, HttpStatus.OK);
        } catch (LaboratoryNotFoundException | TaskNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (SectionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/{sectionId}")
    public ResponseEntity<Section> getSection(@PathVariable Long laboratoryNumber, @PathVariable Long taskId, @PathVariable Long sectionId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            Section section = sectionService.getSectionFromTask(sectionId, task);
            return new ResponseEntity<>(section, HttpStatus.OK);
        } catch (LaboratoryNotFoundException | TaskNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (SectionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PutMapping
    public ResponseEntity<Section> updateSection(@RequestBody Section section, @PathVariable Long laboratoryNumber, @PathVariable Long taskId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            section.setTask(task);
            sectionService.updateSection(section);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TaskNotFoundException | LaboratoryNotFoundException | SectionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/{sectionId}")
    public ResponseEntity<Section> deleteSection(@PathVariable Long laboratoryNumber, @PathVariable Long taskId, @PathVariable Long sectionId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            Section section = sectionService.getSectionFromTask(sectionId, task);
            sectionService.deleteSection(section.getId());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TaskNotFoundException | LaboratoryNotFoundException | SectionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/{sectionId}/code")
    public ResponseEntity<Section> updateSectionCode(@RequestBody String code, @PathVariable Long laboratoryNumber, @PathVariable Long taskId, @PathVariable Long sectionId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            Section section = sectionService.getSectionFromTask(sectionId, task);
            section.setCode(code);
            sectionService.updateSection(section);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TaskNotFoundException | LaboratoryNotFoundException | SectionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/{sectionId}/code")
    public ResponseEntity<String> getSectionCode(@PathVariable Long laboratoryNumber, @PathVariable Long taskId, @PathVariable Long sectionId) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            Task task = taskService.getTaskForLaboratory(taskId, laboratory);
            Section section = sectionService.getSectionFromTask(sectionId, task);
            String code = section.getCode();
            if (code == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(code, HttpStatus.OK);
            }
        } catch (LaboratoryNotFoundException | TaskNotFoundException | SectionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
