package pl.java.projekt.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.java.projekt.rest.exception.LaboratoryAlreadyExistsException;
import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.service.LaboratoryService;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
@RestController
@CrossOrigin
@RequestMapping("/laboratory")
public class LaboratoryController {

    @Autowired
    LaboratoryService laboratoryService;

    @PostMapping
    public ResponseEntity<Laboratory> addLaboratory(@RequestBody Laboratory laboratory) {

        try {
            laboratoryService.addNewLaboratory(laboratory);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (LaboratoryAlreadyExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }

    @GetMapping
    public ResponseEntity<List<Laboratory>> getAllLaboratories() {

        try {
            List<Laboratory> laboratories = laboratoryService.getAllLaboratories();
            return new ResponseEntity<>(laboratories, HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/{laboratoryNumber}")
    public ResponseEntity<Laboratory> getLaboratory(@PathVariable Long laboratoryNumber) {

        try {
            Laboratory laboratory = laboratoryService.getLaboratory(laboratoryNumber);
            return new ResponseEntity<>(laboratory, HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PutMapping
    public ResponseEntity<Laboratory> updateLaboratory(@RequestBody Laboratory laboratory) {

        try {
            laboratoryService.updateLaboratory(laboratory);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/{laboratoryNumber}")
    public ResponseEntity<Laboratory> deleteLaboratory(@PathVariable Long laboratoryNumber) {

        try {
            laboratoryService.deleteLaboratory(laboratoryNumber);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (LaboratoryNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
