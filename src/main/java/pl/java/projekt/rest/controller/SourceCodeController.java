package pl.java.projekt.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

/**
 * Created by Łuaksz Patro
 * on 24.06.17.
 */
@RestController
@CrossOrigin
@RequestMapping("/sourcecode")
public class SourceCodeController {

    @PostMapping("/escape")
    public ResponseEntity<String> escapeSourceCode(@RequestBody(required = false) String body) {

        if (body == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HtmlUtils.htmlEscape(body), HttpStatus.OK);

    }

    @PostMapping("/unescape")
    public ResponseEntity<String> unescapeSourceCode(@RequestBody(required = false) String body) {

        if (body == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HtmlUtils.htmlUnescape(body), HttpStatus.OK);

    }


}
