package pl.java.projekt.rest.model.repository;

import org.springframework.data.repository.CrudRepository;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.Task;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public interface TaskRepository extends CrudRepository<Task, Long> {

    @Override
    Task findOne(Long id);

    //Task findByIdAndLaboratory_Number(Long id, Long laboratoryNumber);

    Task findByIdAndLaboratory(Long id, Laboratory laboratory);

    List<Task> findAllByLaboratoryOrderByDifficultyLevel(Laboratory lab);

}
