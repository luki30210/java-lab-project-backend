package pl.java.projekt.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
@Entity
@Table(name = "laboratories")
public class Laboratory {

    @Id
    Long number;

    @NotNull
    String subject;

    @JsonIgnore
    @OneToMany(mappedBy = "laboratory", cascade = CascadeType.ALL)
    List<Task> tasks;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
