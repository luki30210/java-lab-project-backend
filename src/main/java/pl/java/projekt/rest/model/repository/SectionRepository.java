package pl.java.projekt.rest.model.repository;

import org.springframework.data.repository.CrudRepository;
import pl.java.projekt.rest.model.Section;
import pl.java.projekt.rest.model.Task;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public interface SectionRepository extends CrudRepository<Section, Long> {

    @Override
    Section findOne(Long id);

    List<Section> findAllByTask(Task task);

    Section findByIdAndTask(Long id, Task task);

}
