package pl.java.projekt.rest.model.service;

import pl.java.projekt.rest.exception.LaboratoryAlreadyExistsException;
import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public interface LaboratoryService {

    Laboratory getLaboratory(Long number) throws LaboratoryNotFoundException;

    List<Laboratory> getAllLaboratories() throws LaboratoryNotFoundException;

    void addNewLaboratory(Laboratory laboratory) throws LaboratoryAlreadyExistsException;

    void updateLaboratory(Laboratory laboratory) throws LaboratoryNotFoundException;

    void deleteLaboratory(Long laboratoryNumber) throws LaboratoryNotFoundException;

}
