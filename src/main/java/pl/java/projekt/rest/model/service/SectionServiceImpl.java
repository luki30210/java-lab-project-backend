package pl.java.projekt.rest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.projekt.rest.exception.SectionNotFoundException;
import pl.java.projekt.rest.model.Section;
import pl.java.projekt.rest.model.Task;
import pl.java.projekt.rest.model.repository.SectionRepository;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
@Service
public class SectionServiceImpl implements SectionService {

    @Autowired
    SectionRepository sectionRepository;

    @Override
    public Section getSectionFromTask(Long sectionId, Task task) throws SectionNotFoundException {

        Section section = sectionRepository.findByIdAndTask(sectionId, task);

        if (section == null) {
            throw new SectionNotFoundException(sectionId);
        }

        return section;

    }

    @Override
    public List<Section> getAllSectionsFromTask(Task task) throws SectionNotFoundException {

        List<Section> sections = sectionRepository.findAllByTask(task);

        if (sections.isEmpty()) {
            throw new SectionNotFoundException("No section found.");
        }

        return sections;

    }

    @Override
    public void addNewSection(Section section) {

        sectionRepository.save(section);

    }

    @Override
    public void updateSection(Section section) throws SectionNotFoundException {

        Long sectionId = section.getId();
        Section sectionOld = sectionRepository.findByIdAndTask(sectionId, section.getTask());

        if (sectionOld == null) {
            throw new SectionNotFoundException(sectionId);
        } else {
            sectionOld.setName(section.getName());
            //sectionOld.setTask(section.getTask());    //task pozostanie niezmieniony
            sectionOld.setCode(section.getCode());
            sectionRepository.save(sectionOld);
        }

    }

    @Override
    public void deleteSection(Long sectionId) throws SectionNotFoundException {

        Section sectionOld = sectionRepository.findOne(sectionId);

        if (sectionOld == null) {
            throw new SectionNotFoundException(sectionId);
        }

        sectionRepository.delete(sectionId);

    }

}
