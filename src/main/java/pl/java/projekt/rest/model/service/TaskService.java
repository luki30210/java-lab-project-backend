package pl.java.projekt.rest.model.service;

import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.exception.TaskNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.Task;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public interface TaskService {

    Task getTaskForLaboratory(Long id, Laboratory laboratory) throws TaskNotFoundException;

    List<Task> getAllTasksForLaboratory(Laboratory laboratory) throws TaskNotFoundException;

    void addNewTask(Task task);

    void updateTask(Task task) throws TaskNotFoundException;

    void deleteTask(Long taskId) throws TaskNotFoundException;

}
