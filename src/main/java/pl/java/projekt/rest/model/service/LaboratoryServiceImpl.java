package pl.java.projekt.rest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.projekt.rest.exception.LaboratoryAlreadyExistsException;
import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.repository.LaboratoryRepository;

import java.util.List;

/**
 * Created by Łukasz Patro on 24.06.17.
 */
@Service
public class LaboratoryServiceImpl implements LaboratoryService {

    @Autowired
    LaboratoryRepository laboratoryRepository;

    @Override
    public Laboratory getLaboratory(Long number) throws LaboratoryNotFoundException {

        Laboratory laboratory = laboratoryRepository.findOne(number);

        if (laboratory == null) {
            throw new LaboratoryNotFoundException(number);
        }

        return laboratory;

    }

    @Override
    public List<Laboratory> getAllLaboratories() throws LaboratoryNotFoundException {

        List<Laboratory> laboratories = laboratoryRepository.findAll();

        if (laboratories.isEmpty()) {
            throw new LaboratoryNotFoundException("No laboratory found.");
        }

        return laboratories;

    }

    @Override
    public void addNewLaboratory(Laboratory laboratory) throws LaboratoryAlreadyExistsException {

        Long laboratoryNumber = laboratory.getNumber();

        if (laboratoryRepository.findOne(laboratoryNumber) != null) {
            throw new LaboratoryAlreadyExistsException(laboratoryNumber);
        } else {
            laboratoryRepository.save(laboratory);
        }

    }

    @Override
    public void updateLaboratory(Laboratory laboratory) throws LaboratoryNotFoundException {

        Long laboratoryNumber = laboratory.getNumber();

        if (laboratoryRepository.findOne(laboratoryNumber) == null) {
            throw new LaboratoryNotFoundException(laboratoryNumber);
        } else {
            laboratoryRepository.save(laboratory);
        }

    }

    @Override
    public void deleteLaboratory(Long laboratoryNumber) throws LaboratoryNotFoundException {

        if (laboratoryRepository.findOne(laboratoryNumber) == null) {
            throw new LaboratoryNotFoundException(laboratoryNumber);
        } else {
            laboratoryRepository.delete(laboratoryNumber);
        }

    }

}
