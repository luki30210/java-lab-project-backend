package pl.java.projekt.rest.model.service;

import pl.java.projekt.rest.exception.SectionNotFoundException;
import pl.java.projekt.rest.model.Section;
import pl.java.projekt.rest.model.Task;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public interface SectionService {

    Section getSectionFromTask(Long sectionId, Task task) throws SectionNotFoundException;

    List<Section> getAllSectionsFromTask(Task task) throws SectionNotFoundException;

    void addNewSection(Section section);

    void updateSection(Section section) throws SectionNotFoundException;

    void deleteSection(Long sectionId) throws SectionNotFoundException;

}
