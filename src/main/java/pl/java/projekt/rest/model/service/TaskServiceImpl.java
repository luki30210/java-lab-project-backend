package pl.java.projekt.rest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.java.projekt.rest.exception.LaboratoryNotFoundException;
import pl.java.projekt.rest.exception.TaskNotFoundException;
import pl.java.projekt.rest.model.Laboratory;
import pl.java.projekt.rest.model.Task;
import pl.java.projekt.rest.model.repository.TaskRepository;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public Task getTaskForLaboratory(Long id, Laboratory laboratory) throws TaskNotFoundException {

        Task task = taskRepository.findByIdAndLaboratory(id, laboratory);

        if (task == null) {
            throw new TaskNotFoundException(id);
        }

        return task;

    }

    @Override
    public List<Task> getAllTasksForLaboratory(Laboratory laboratory) throws TaskNotFoundException {

        List<Task> tasks = taskRepository.findAllByLaboratoryOrderByDifficultyLevel(laboratory);

        if (tasks.isEmpty()) {
            throw new TaskNotFoundException("No task found.");
        }

        return tasks;

    }

    @Override
    public void addNewTask(Task task) {

        taskRepository.save(task);

    }

    @Override
    public void updateTask(Task task) throws TaskNotFoundException {

        Long taskId = task.getId();
        Task taskOld = taskRepository.findByIdAndLaboratory(taskId, task.getLaboratory());

        if (taskOld == null) {
            throw new TaskNotFoundException(taskId);
        } else {
            taskOld.setDifficultyLevel(task.getDifficultyLevel());
            //taskOld.setLaboratory(laboratory);
            taskOld.setName(task.getName());
            taskOld.setSections(task.getSections());
            taskRepository.save(taskOld);
        }

    }

    @Override
    public void deleteTask(Long taskId) throws TaskNotFoundException {

        Task taskOld = taskRepository.findOne(taskId);

        if (taskOld == null) {
            throw new TaskNotFoundException(taskId);
        } else {
            taskRepository.delete(taskId);
        }

    }

}
