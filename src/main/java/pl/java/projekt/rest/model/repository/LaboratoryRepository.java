package pl.java.projekt.rest.model.repository;

import org.springframework.data.repository.CrudRepository;
import pl.java.projekt.rest.model.Laboratory;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public interface LaboratoryRepository extends CrudRepository<Laboratory, Long> {

    @Override
    Laboratory findOne(Long number);

    List<Laboratory> findAll();

}
